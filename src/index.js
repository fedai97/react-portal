import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './portals/MainPortal';
import StyleFindBlock from './portals/StyleFindBlockPortal';
import * as serviceWorker from './serviceWorker';
import { Provider } from 'react-redux';
import { configureStore } from './store';

const MainPortal = () => ReactDOM.createPortal(<Main />, document.getElementById('main'));
const StyleFindBlockPortal = () => ReactDOM.createPortal(<StyleFindBlock />, document.getElementById('search-block'));

const store = configureStore();

ReactDOM.render(
    <Provider store={store}>
        <MainPortal />
        <StyleFindBlockPortal />
    </Provider>,
    document.getElementById('root')
);

serviceWorker.unregister();
