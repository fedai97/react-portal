import React from 'react';
import {Lines} from 'react-preloaders';
import Moment from 'react-moment';
import 'moment-timezone';
import Offerblock from './Offerblock.jsx';
import Offerdetailblock from './Offerdetailblock.jsx';
import Racedetailblock from './Racedetailblock';
import Racedetailblockback from './Racedetailblockback';
import '../Styles/style.css';
import '../Styles/styleShowBlock.css';
import '../Styles/teststyle.css';
import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";
import MomentLocaleUtils, {
    formatDate,
    parseDate,
} from 'react-day-picker/moment';
import 'moment/locale/ru';
import moment from 'moment';

//import 'moment/min/moment-with-locales'
import axios from 'axios';
import Autocomplete from 'react-autocomplete';
import Switch from "react-switch";
import "react-loader-spinner/dist/loader/css/react-spinner-loader.css";
import {bindActionCreators} from "redux";
import * as mainAction from "../store/actions/main";
import {connect} from "react-redux";
import {logger} from "redux-logger/src";

class Finder extends React.Component {
    constructor(props) {

        super(props);
        this.state = {
            // from: 'Киев',
            showMe: true,
            // currency: 'UAH',
            modalUserProfileClass: "modal fade",
            modalUserProfileStyle: 'none',
            // resErrorRace2: false,
            noPlaceNum: 1,
            showSeatsBlock: false,
            showRegOffer: false,
            offerShow: false,
            modalUserLoginClass: "modal fade",
            modalUserLoginStyle: 'none',
            modalUserRegisterClass: "modal fade",
            modalUserRegisterStyle: 'none',
            modalUserTicketsClass: "modal fade",
            modalUserTicketsClassTwo: "modal fade",
            modalUserTicketsStyle: 'none',
            modalUserTicketsStyleTwo: 'none',
            // fromID: 2,
            displayLogin: 'none',
            errorLogin: '',
            //error block
            erLogin: 'errorLoginNone',
            erPassword: 'errorPasswordNone',
            erSurname: 'errorSurnameNone',
            erName: 'errorNameNone',
            erEmail: 'errorEmailNone',
            erPhone: 'errorPhoneNone',
            //end error
            // toID: 1,
            userID: 0,
            minusone: new Date(),
            value: '',
            testValue: '',
            // visibleCalendar: 'none',
            // direct: false,
            // goback: false,
            loading: true,
            // to: 'Варшава',
            displayCalendar: 'none',
            // showByTic: 'none',
            // blockShow: 'block',
            offerCheckItUp: 'block',
            // on: new Date(),
            // onback: new Date(),
            // passengers: '1',
            // cityList: [],
            matrix: [],
            promoArr: [],
            seatsSelect: [],
            ticketsInfo: [],
            // tripList: [],
            // tripListTwo: [],
            // tripId: '',
            // nextDate: '',
            raceId: '',
            // dataCalendar: [],
            //data for register
            login: "",
            password: "",
            surname: "",
            nameTest: "",
            email: "",
            phone: "",
            //end data for register
            //data gor auth
            loginAuth: "",
            passwordAuth: "",
            erAuth: "erAuthNone",
            //end data for auth
            showSign: true,
            showReg: false,
            showBlockNone: false,
            showBlockTickets: false,
            showBlockProfile: false,
            // loadingTwo: false,
            NameUser: "Вход/Регистрация",
            sessionUserActiv: [],
            //переменные для сесии
            nameS: "",
            surnameS: "",
            passwordS: "",
            password2S: "",
            emailS: "",
            phoneS: "",
            //end session
            arrTic: {},
            arrTicTwo: [],
            arrBy: {},
            infForLiq: [],
            priceBy: '',
            promoCodeName: '',
            currNameBy: '',
            // resErrorRace: false,
            // sortMenu: false,
            tripInfForBy: '',
            stDepAddrInfForBy: '',
            stArrNameInfForBy: '',
            DepInfForBy: '',
            ArrInfForBy: '',
            DepSort: '',
            ArrSort: '',
            wayTimeSort: '',
            lengPass: [],
            butPayTic: "",
            butPayTicTwo: "",
            showButPayTic: false,
            checkboxOffer: false,
            showButPayTicTwo: false,
            forSortDownAndUp: false,
            showBuyNext: true,
            showBuyNextTwo: true,
            inflengPass: [],
            arrForOctobBy: [],
            userAr: [],
            promoInput: "",
            nameSBlockClass: 'form-group',
            seatBlockClass: 'form-group',
            surnameSBlockClass: 'form-group',
            phoneSBlockClass: 'form-group',
            emailsBlockClass: 'form-group',
            nameSErrorBlock: 'none',
            seatErrorBlock: 'none',
            surnameSErrorBlock: 'none',
            phoneSErrorBlock: 'none',
            emailSErrorBlock: 'none',
            seatsRows: 0,
            seatsColumns: 0,
            buyId: '',
            buyIdTwo: '',
            reserveObj: [],
            reserveObjInfo: [],
            reserveObjInfoExtended: [],
            mSum: [],
            confirmReserveObj: [],
            orderObj: [],
            lgot: "",
            ticketId: "",
            ticketIdT: [],
            offer: '',
            showDetails: '',
            // showFindBlock: true,


        };
        this.handleChange = this.handleChange.bind(this);
        this.handleCheck = this.handleCheck.bind(this);
        this.handleOffer = this.handleOffer.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.infoBlockBack = this.infoBlockBack.bind(this);
        this.displayCalendar = this.displayCalendar.bind(this);
        this.handleChangeDateArrow = this.handleChangeDateArrow.bind(this);
        this.selectPlace = this.selectPlace.bind(this);
        this.changePromocode = this.changePromocode.bind(this);
        this.changeDateOnCalendar = this.changeDateOnCalendar.bind(this);
        this.signFunction = this.signFunction.bind(this);
        this.regFunction = this.regFunction.bind(this);
        this.subDataForReg = this.subDataForReg.bind(this);
        this.subDataForAuth = this.subDataForAuth.bind(this);
        this.showBlockRegister = this.showBlockRegister.bind(this);
        this.showBlockLogin = this.showBlockLogin.bind(this);
        this.sessionUser = this.sessionUser.bind(this);
        this.ticketsInfoFunction = this.ticketsInfoFunction.bind(this);
        this.ticketsInfoFunctionTwo = this.ticketsInfoFunctionTwo.bind(this);
        this.handleSaveProfile = this.handleSaveProfile.bind(this);
        this.dataSessionUser = this.dataSessionUser.bind(this);
        this.deleteLocal = this.deleteLocal.bind(this);
        this.showBlockProfileFunction = this.showBlockProfileFunction.bind(this);
        this.CloseModalUserProfile = this.CloseModalUserProfile.bind(this);
        this.CloseModalUserTickets = this.CloseModalUserTickets.bind(this);
        this.CloseModalUserTicketsTwo = this.CloseModalUserTicketsTwo.bind(this);
        this.CloseModalLogin = this.CloseModalLogin.bind(this);
        this.CloseModalRegister = this.CloseModalRegister.bind(this);
        this.showBlockTicketsFunction = this.showBlockTicketsFunction.bind(this);
        this.showBlockTicketsFunctionTwo = this.showBlockTicketsFunctionTwo.bind(this);
        this.setNextDate = this.setNextDate.bind(this);
        //заказ билета
        this.nameS = this.nameS.bind(this);
        this.passwordS = this.passwordS.bind(this);
        this.password2S = this.password2S.bind(this);
        this.surnameS = this.surnameS.bind(this);
        this.showSeats = this.showSeats.bind(this);
        this.emailS = this.emailS.bind(this);
        this.phoneS = this.phoneS.bind(this);
        this.showByTic = this.showByTic.bind(this);
        this.backStup = this.backStup.bind(this);
        this.PayArr = this.PayArr.bind(this);
        this.PayArrTwo = this.PayArrTwo.bind(this);
        this.loadin = this.loadin.bind(this);
        this.promocodeRequest = this.promocodeRequest.bind(this);
        this.prePromocodeRequest = this.prePromocodeRequest.bind(this);
        this.sortDepartureUp = this.sortDepartureUp.bind(this);
        this.sortDepartureDown = this.sortDepartureDown.bind(this);
        this.sortArriveUp = this.sortArriveUp.bind(this);
        this.sortArriveDown = this.sortArriveDown.bind(this);
        this.sortWayTimeUp = this.sortWayTimeUp.bind(this);
        this.sortWayTimeDown = this.sortWayTimeDown.bind(this);
        this.sortPriceUp = this.sortPriceUp.bind(this);
        this.sortPriceDown = this.sortPriceDown.bind(this);
        this.sortSortDep = this.sortSortDep.bind(this);
        this.sortSortArr = this.sortSortArr.bind(this);
        this.sortSortWay = this.sortSortWay.bind(this);
        this.sortSortPrice = this.sortSortPrice.bind(this);
        this.setReserve = this.setReserve.bind(this);
        this.pushnameS = this.pushnameS.bind(this);
        this.pushphoneS = this.pushphoneS.bind(this);
        this.pushsurnameS = this.pushsurnameS.bind(this);

    }


    setReserve(name, surname, email, phone) {

        let reserveObj = this.state.reserveObj;
        let userid = localStorage.userID;

        let nameS = this.state.nameS;
        let surnameS = this.state.surnameS;
        let emailS = this.state.emailS;
        let phoneS = this.state.phoneS;
        let passwordS = this.state.passwordS;
        let userAr = this.state.userAr;


        let we_can_go = true;

        if (this.state.nameS == '') {
            we_can_go = false;
            this.setState({
                nameSErrorBlock: 'block',
                nameSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.noPlaceNum === 0) {
            if (this.props.passengers != this.state.seatsSelect.length) {
                this.setState({
                    showSeatsBlock: true,
                    seatErrorBlock: 'block',
                    seatBlockClass: 'form-group has-error',
                });
            }
        }
        if (this.state.surnameS == '') {
            we_can_go = false;
            this.setState({
                surnameSErrorBlock: 'block',
                surnameSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.emailS == '') {
            we_can_go = false;
            this.setState({
                emailSErrorBlock: 'block',
                emailSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.phoneS == '') {
            we_can_go = false;
            this.setState({
                phoneSErrorBlock: 'block',
                phoneSBlockClass: 'form-group has-error',
            });
        }
        let _userAr = this.state.userAr;
        for (let i = 0; i < _userAr.length; i++) {
            if (_userAr[i].nameS == '') {
                _userAr[i].blocknameS = 'form-group has-error';
                _userAr[i].labelnameS = 'block';
                we_can_go = false;
            }
            if (_userAr[i].surnameS == '') {
                _userAr[i].blocksurnameS = 'form-group has-error';
                _userAr[i].labelsurnameS = 'block';
                we_can_go = false;
            }
            if (_userAr[i].phoneS == '') {
                _userAr[i].blockphoneS = 'form-group has-error';
                _userAr[i].labelphoneS = 'block';
                we_can_go = false;
            }
        }
        this.setState({
            userAr: _userAr
        });

        if (!we_can_go) {
            return false;
        }
        let arrBy = this.state.arrBy;
        arrBy.name = name;
        arrBy.surname = surname;
        arrBy.email = email;
        arrBy.phone = phone;

        let infForLiq1 = this.state.infForLiq;
        infForLiq1.arrBy = arrBy;
        infForLiq1.arrTic = this.state.arrTic;
        //массив для покупки в переменной arrTic
        let inflengPass = this.state.inflengPass;

        console.log(this.state.infForLiq, this.state.lengPass);
        axios({
            method: 'post',
            url: 'http://new.viabona.com.ua/api/index.php/api/pay/makereserve',
            data: {
                state: this.state,
                userid: userid,
            }
        }).then(res => {
            console.log(res.data);
            this.setState({
                reserveObj: res.data,
                //showButPayTic: true,
                //butPayTic: res.data,
                showBuyNext: false

            });
            alert("Билет успешно забронирован!");
        });

        //console.log(this.state.nameS);
        //console.log(this.state.userAr);
        console.log(this.state.reserveObj);
    }

    sortSortDep() {
        if (this.state.forSortDownAndUp) {
            this.sortDepartureUp();
        } else {
            this.sortDepartureDown();
        }
    }

    sortDepartureUp() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (a.dtDepSec > b.dtDepSec) return 1;
            if (a.dtDepSec < b.dtDepSec) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortDepartureDown() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (b.dtDepSec > a.dtDepSec) return 1;
            if (b.dtDepSec < a.dtDepSec) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortSortArr() {
        if (this.state.forSortDownAndUp) {
            this.sortArriveUp();
        } else {
            this.sortArriveDown();
        }
    }

    sortArriveUp() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (a.dtArrSec > b.dtArrSec) return 1;
            if (a.dtArrSec < b.dtArrSec) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortArriveDown() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (b.dtArrSec > a.dtArrSec) return 1;
            if (b.dtArrSec < a.dtArrSec) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortSortWay() {
        if (this.state.forSortDownAndUp) {
            this.sortWayTimeUp();
        } else {
            this.sortWayTimeDown();
        }
    }

    sortWayTimeUp() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (a.wayTimeSec > b.wayTimeSec) return 1;
            if (a.wayTimeSec < b.wayTimeSec) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortWayTimeDown() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (b.wayTimeSec > a.wayTimeSec) return 1;
            if (b.wayTimeSec < a.wayTimeSec) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortSortPrice() {
        if (this.state.forSortDownAndUp) {
            this.sortPriceUp();
        } else {
            this.sortPriceDown();
        }
    }

    sortPriceUp() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (a.price > b.price) return 1;
            if (a.price < b.price) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    sortPriceDown() {
        let _tripList = this.props.tripList;

        function compareNumeric(a, b) {
            if (b.price > a.price) return 1;
            if (b.price < a.price) return -1;
        }

        _tripList.sort(compareNumeric);
        this.props.mainAction.setTripList(_tripList);
        // this.setState({_tripList: _tripList});
        this.setState({forSortDownAndUp: !this.state.forSortDownAndUp});
    }

    changePromocode(el) {
        this.setState({
            promoInput: el.value
        });
    }

    selectPlace(placeArray, row, place) {
        if (!placeArray.free) {
            //if (!placeArray.free || placeArray.svc) {
            return false;
        }
        // console.log('Ряд:' + row);
        // console.log('Место:' + place);
        // console.log('Место:' + placeArray.n);
        //console.log('Служебное:' + placeArray.svc);
        //console.log('Служебное в матрице:' + this.state.matrix[0][row][place].svc);

        let passengersQuantity = this.props.passengers;
        let seatsSelect = this.state.seatsSelect;
        let _matrix = this.state.matrix;


        //    function setSelect (){
        _matrix[0][row][place].selected = 1;


        seatsSelect.push({'placeArray': _matrix[0][row][place], 'row': row, 'place': place});
        if (seatsSelect.length > passengersQuantity) {
            seatsSelect.splice(0, passengersQuantity);
        }
        _matrix[0].forEach(function (item, index, array) {
            item.forEach(function (it, ind, arr) {
                _matrix[0][index][ind].selected = false;
            });
        });
        seatsSelect.map((el, i) => {
            _matrix[0][el.row][el.place].selected = 1;
        });
        this.setState({
            matrix: _matrix,
            seatsSelect: seatsSelect,
        });
    }

    handleSaveProfile(password = false) {
        let $request = {
            name: this.state.nameS,
            surname: this.state.surnameS,
            email: this.state.emailS,
            phone: this.state.phoneS,
            id: localStorage.userID
        };
        let $endpoint = 'http://new.viabona.com.ua/api/index.php/api/octobus/save_profile';
        if (password) {
            if (this.state.passwordS != this.state.password2S) {
                alert('Пароли не совспадают!');
                return false;
            }
            $request = {
                password: this.state.passwordS,
                id: localStorage.userID
            }
            $endpoint = 'http://new.viabona.com.ua/api/index.php/api/octobus/save_password';
        }
        axios({
            method: 'post',
            url: $endpoint,
            data: $request
        }).then(res => {
            console.log(res.data);
            alert('Данные успешно обновлены');
            localStorage.name = this.state.nameS;
            localStorage.surname = this.state.surnameS;
            localStorage.email = this.state.emailS;
            localStorage.phone = this.state.phoneS;
        });
    }

    PayArr(name, surname, email, phone) {

        let we_can_go = true;
        if (this.state.nameS == '') {
            we_can_go = false;
            this.setState({
                nameSErrorBlock: 'block',
                nameSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.noPlaceNum === 0) {
            if (this.props.passengers != this.state.seatsSelect.length) {
                this.setState({
                    showSeatsBlock: true,
                    seatErrorBlock: 'block',
                    seatBlockClass: 'form-group has-error',
                });
            }
        }
        if (this.state.surnameS == '') {
            we_can_go = false;
            this.setState({
                surnameSErrorBlock: 'block',
                surnameSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.emailS == '') {
            we_can_go = false;
            this.setState({
                emailSErrorBlock: 'block',
                emailSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.phoneS == '') {
            we_can_go = false;
            this.setState({
                phoneSErrorBlock: 'block',
                phoneSBlockClass: 'form-group has-error',
            });
        }
        let _userAr = this.state.userAr;
        for (let i = 0; i < _userAr.length; i++) {
            if (_userAr[i].nameS == '') {
                _userAr[i].blocknameS = 'form-group has-error';
                _userAr[i].labelnameS = 'block';
                we_can_go = false;
            }
            if (_userAr[i].surnameS == '') {
                _userAr[i].blocksurnameS = 'form-group has-error';
                _userAr[i].labelsurnameS = 'block';
                we_can_go = false;
            }
            if (_userAr[i].phoneS == '') {
                _userAr[i].blockphoneS = 'form-group has-error';
                _userAr[i].labelphoneS = 'block';
                we_can_go = false;
            }
        }
        this.setState({
            userAr: _userAr
        });

        if (!we_can_go) {
            return false;
        }
        let arrBy = this.state.arrBy;
        arrBy.name = name;
        arrBy.surname = surname;
        arrBy.email = email;
        arrBy.phone = phone;

        let infForLiq1 = this.state.infForLiq;
        infForLiq1.arrBy = arrBy;
        infForLiq1.arrTic = this.state.arrTic;
        //массив для покупки в переменной arrTic
        let inflengPass = this.state.inflengPass;

        console.log(this.state.infForLiq, this.state.lengPass);
        axios({
            method: 'post',
            url: 'http://new.viabona.com.ua/api/index.php/api/pay/pay',
            data: {
                state: this.state
            }
        }).then(res => {
            console.log(res.data);
            this.setState({
                showButPayTic: true,
                butPayTic: res.data,
                showBuyNext: false

            })

        });

    }


    PayArrTwo(name, surname, email, phone) {

        let userid = localStorage.userID;
        let we_can_go = true;
        if (this.state.nameS == '') {
            we_can_go = false;
            this.setState({
                nameSErrorBlock: 'block',
                nameSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.noPlaceNum === 0) {
            if (this.props.passengers != this.state.seatsSelect.length) {
                this.setState({
                    showSeatsBlock: true,
                    seatErrorBlock: 'block',
                    seatBlockClass: 'form-group has-error',
                });
            }
        }
        if (this.state.surnameS == '') {
            we_can_go = false;
            this.setState({
                surnameSErrorBlock: 'block',
                surnameSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.emailS == '') {
            we_can_go = false;
            this.setState({
                emailSErrorBlock: 'block',
                emailSBlockClass: 'form-group has-error',
            });
        }
        if (this.state.phoneS == '') {
            we_can_go = false;
            this.setState({
                phoneSErrorBlock: 'block',
                phoneSBlockClass: 'form-group has-error',
            });
        }
        let _userAr = this.state.userAr;
        for (let i = 0; i < _userAr.length; i++) {
            if (_userAr[i].nameS == '') {
                _userAr[i].blocknameS = 'form-group has-error';
                _userAr[i].labelnameS = 'block';
                we_can_go = false;
            }
            if (_userAr[i].surnameS == '') {
                _userAr[i].blocksurnameS = 'form-group has-error';
                _userAr[i].labelsurnameS = 'block';
                we_can_go = false;
            }
            if (_userAr[i].phoneS == '') {
                _userAr[i].blockphoneS = 'form-group has-error';
                _userAr[i].labelphoneS = 'block';
                we_can_go = false;
            }
        }
        this.setState({
            userAr: _userAr
        });

        if (!we_can_go) {
            return false;
        }
        let arrBy = this.state.arrBy;
        arrBy.name = name;
        arrBy.surname = surname;
        arrBy.email = email;
        arrBy.phone = phone;

        let infForLiq1 = this.state.infForLiq;
        infForLiq1.arrBy = arrBy;
        infForLiq1.arrTic = this.state.arrTic;
        //массив для покупки в переменной arrTic
        let inflengPass = this.state.inflengPass;

        console.log(this.state.infForLiq, this.state.lengPass);
        axios({
            method: 'post',
            url: 'http://new.viabona.com.ua/api/index.php/api/pay/confirm',
            data: {
                state: this.state,
                userid: userid,
            }
        }).then(res => {
            console.log(res.data);
            this.setState({
                showButPayTicTwo: true,
                butPayTicTwo: res.data,
                showBuyNextTwo: false

            })

        });

    }

    backStup() {
        this.props.mainAction.setShowFindBlock(true);
        this.props.mainAction.setBlockShow('block');
        this.props.mainAction.setShowByTic('none');

        this.setState({
            // showByTic: 'none',
            // blockShow: 'block',
            showDetails: '',
            // showFindBlock: true,

        })
        //document.getElementById('someText').style.display = "block";
    }

    showByTic(price, currency, trip, dtArr, stArrName, stDepName, dtDep, stDepAddr, stArrAddr, pass, currName, raceId, tripId, dtDepSec, dtArrSec, wayTimeSec, key) {
        this.props.mainAction.setShowFindBlock(false);
        this.props.mainAction.setBlockShow('none');
        this.props.mainAction.setShowByTic('block');

        this.setState({
            // showByTic: 'block',
            // blockShow: 'none',
            showDetails: 'block',
            // showFindBlock: false,
        });
        let arrUser = this.state.arrTic;
        arrUser.price = price;
        arrUser.currency = currency;
        arrUser.trip = trip;
        arrUser.dtArr = dtArr;
        arrUser.stArrName = stArrName;
        arrUser.stDepName = stDepName;
        arrUser.dtDep = dtDep;
        arrUser.stDepAddr = stDepAddr;
        arrUser.stArrAddr = stArrAddr;
        arrUser.passengers = pass;
        arrUser.currName = currName;
        arrUser.raceId = raceId;
        arrUser.tpripId = tripId;
        arrUser.dtDepSec = dtDepSec;
        arrUser.dtArrSec = dtArrSec;
        arrUser.wayTimeSec = wayTimeSec;
        this.props.mainAction.setTripId(tripId);

        this.setState({
            priceBy: price * pass,
            currNameBy: currName,
            tripInfForBy: trip,
            stDepAddrInfForBy: stDepAddr,
            stArrNameInfForBy: stArrName,
            DepInfForBy: dtDep,
            ArrInfForBy: dtArr,
            // tripId: tripId,
            raceId: raceId,
            DepSort: dtDepSec,
            ArrSort: dtArrSec,
            wayTimeSort: wayTimeSec,
            arrTicTwo: [arrUser],
        });


        let lengPass = this.state.lengPass;
        let userAr = [];
        for (let i = 1; i < this.props.passengers; i++) {
            let kkey = i + 1;
            userAr.push({
                'nameS': '',
                'kkey': kkey,
                'surnameS': '',
                'phoneS': '',
                'blocknameS': 'form-group',
                'blocksurnameS': 'form-group',
                'blockphoneS': 'form-group',
                'labelnameS': 'none',
                'labelsurnameS': 'none',
                'labelphoneS': 'none'
            });
        }
        this.setState({userAr: userAr});

        console.log(lengPass);
        console.log(this.state.arrTic);

        let lgot = this.state.lgot;
        let tripMatrix = this.state.matrix;
        // console.log(tripMatrix);
        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/tripMatrix?tripId=' + tripId).then(res => {
            //tripMatrix.matrix = res.data;
            console.log(res.data);
            this.setState({
                noPlaceNum: parseInt(res.data[0].noPlaceNum)
            });
            if (parseInt(res.data[0].noPlaceNum) === 1) {
                this.setState({showSeatsBlock: 'none'});
            } else {
                this.setState({matrix: res.data[0].busTempl.matrix});
                this.setState({seatsRows: res.data[0].busTempl.size[0].x});
                this.setState({seatsColumns: res.data[0].busTempl.size[0].y});
                //console.log(this.state.seatsRows);
                //console.log(this.state.seatsColumns);
            }

        });

        //document.getElementsByClassName('fusion-fullwidth.fullwidth-box.fusion-builder-row-2.someText.nonhundred-percent-fullwidth.non-hundred-percent-height-scrolling').style.display = "none";
        // document.getElementById('someText').style.display = "none";
        //document.body.style.display = "none";
        //document.body.setAttribute('display', 'none');
        //this.loremIpsumF();

    }

    prePromocodeRequest(e) {
        let promoInput = this.state.promoInput;
        this.setState({
            promoInput: e.target.value,
        });
        //console.log(promoInput);
    }

    promocodeRequest() {
        //let promoArr = this.state.promoArr;
        console.log(localStorage);
        if (!localStorage.userID) {
            this.setState({showRegOffer: false});
        } else if (localStorage.userID) {
            let user_id = localStorage.userID;
            axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/checkPromo?tripId=' + this.props.tripId + '&buyerid=' + user_id + '&promocode=' + this.state.promoInput).then(res => {
                this.setState({
                    promoArr: res.data,
                    showRegOffer: false,
                });
                if (res.data[0].applicable === 1) {
                    let new_price = parseFloat(res.data[0].autoLgot.price);
                    new_price = new_price * parseInt(this.props.passengers);
                    this.setState({
                        priceBy: new_price,
                        promoCodeName: "Использовано: " + res.data[0].autoLgot.name,
                    });

                }
                console.log(this.state.promoArr);
            });
            //console.log(promoArr);

        }
    }

    showSeats(matrix) {
        if (this.state.noPlaceNum === 1) {
            return false
        }
        ;
        console.log(matrix[0]);
        matrix[0].map((el, i) => {
            //console.log(el+'array');

            el.map((ryad, ii) => {
                //console.log('asd'+ryad);
                return (
                    <div style={{border: '1px solid black', padding: '5px'}}>{ryad.n}</div>
                )
            });
            return (<br/>)
        })
        this.setState({showSeatsBlock: !this.state.showSeatsBlock});
    }

    pushnameS = id => e => {
        console.log(id);
        console.log(this.state.userAr);
        let userAr = this.state.userAr;
        userAr[id].nameS = e.target.value;
        this.setState({userAr: userAr});
    }
    pushphoneS = id => e => {
        console.log(id);
        console.log(this.state.userAr);
        let userAr = this.state.userAr;
        userAr[id].phoneS = e.target.value;
        this.setState({userAr: userAr});
    }
    pushsurnameS = id => e => {
        console.log(id);
        console.log(this.state.userAr);
        let userAr = this.state.userAr;
        userAr[id].surnameS = e.target.value;
        this.setState({userAr: userAr});
    }

    nameS(el) {
        this.setState({nameS: el.target.value});
    }

    passwordS(el) {
        this.setState({passwordS: el.target.value});
    }

    password2S(el) {
        this.setState({password2S: el.target.value});
    }

    surnameS(el) {
        this.setState({surnameS: el.target.value});
    }

    emailS(el) {
        this.setState({emailS: el.target.value});
    }

    phoneS(el) {
        this.setState({phoneS: el.target.value});
    }

    deleteLocal() {

        localStorage.removeItem("name");
        localStorage.removeItem("surname");
        localStorage.removeItem("email");
        localStorage.removeItem("phone");
        localStorage.removeItem("UserID");
        this.setState({
            nameS: "",
            surnameS: "",
            emailS: "",
            UserID: "",
            phoneS: "",
            ticketsInfo: [],
            reserveObjInfo: [],
        });
        this.setState({
            showBlockTickets: false,
            showBlockProfile: false,
        })

    }

    ticketsInfoFunction(userID) {

        axios.get('http://new.viabona.com.ua/api/index.php/api/pay/newbuyertickets?id=' + userID).then(res => {
            this.setState({
                ticketsInfo: res.data,
            });
        });

    }

    ticketsInfoFunctionTwo() {

        let userid = localStorage.userID;
        let reserveObjInfo = this.state.reserveObjInfo;
        let reserveObjInfoExtended = this.state.reserveObjInfoExtended;
        let preReserveObjInfo = this.state.preReserveObjInfo;
        let mSum = this.state.mSum;


        axios.get('http://new.viabona.com.ua/api/index.php/api/pay/orderinformation?userid=' + userid).then(res => {
            //for(let x = 0; x < 3; x++) {
            if (res.data.tlist) {
                this.setState({
                    //reserveObjInfo: [res.data.tlist[0]],
                    reserveObjInfo: [res.data.tlist[0]],
                    mSum: [res.data.mSum],
                    //reserveObjInfo: preReserveObjInfo,
                });
            }
            //}
        });

        axios.get('http://new.viabona.com.ua/api/index.php/api/pay/orderinformationextended?userid=' + userid).then(res => {
            //for(let x = 0; x < 3; x++) {
            this.setState({
                //reserveObjInfo: [res.data.tlist[0]],
                reserveObjInfoExtended: res.data,
                //reserveObjInfo: preReserveObjInfo,
            });
            //}
        });

        console.log(this.state.reserveObjInfo);
        console.log(this.state.reserveObjInfoExtended);

    }

    CloseModalUserProfile() {
        this.setState({
            modalUserProfileClass: 'modal fade',
            modalUserProfileStyle: 'none'
        });
    }

    CloseModalUserTickets() {
        this.setState({
            modalUserTicketsClass: 'modal fade',
            modalUserTicketsStyle: 'none',
        });
    }

    CloseModalUserTicketsTwo() {
        this.setState({
            modalUserTicketsClassTwo: 'modal fade',
            modalUserTicketsStyleTwo: 'none',
        });
    }

    CloseModalLogin() {
        this.setState({
            modalUserLoginClass: 'modal fade',
            modalUserLoginStyle: 'none'
        });
    }

    CloseModalRegister() {
        this.setState({
            modalUserRegisterClass: 'modal fade',
            modalUserRegisterStyle: 'none'
        });
    }

    showBlockProfileFunction() {
        /* this.setState({
             showBlockProfile: !this.state.showBlockProfile,
             showBlockTickets: false
         });*/
        this.setState({
            modalUserProfileClass: 'modal fade in',
            modalUserProfileStyle: 'block'
        });
    }

    showBlockTicketsFunction() {
        this.ticketsInfoFunction(localStorage.userID);
        this.setState({
            modalUserTicketsClass: 'modal fade in',
            modalUserTicketsStyle: 'block',
        });
    }

    showBlockTicketsFunctionTwo() {
        this.ticketsInfoFunctionTwo(localStorage.userID);
        this.setState({
            modalUserTicketsClassTwo: 'modal fade in',
            modalUserTicketsStyleTwo: 'block',
        });
    }

    showBlockRegister() {
        /* this.setState({
             showBlockNone: !this.state.showBlockNone
         });*/
        this.setState({
            modalUserRegisterClass: 'modal fade in',
            modalUserRegisterStyle: 'block'
        });
    }

    showBlockLogin() {
        /* this.setState({
             showBlockNone: !this.state.showBlockNone
         });*/
        this.setState({
            modalUserLoginClass: 'modal fade in',
            modalUserLoginStyle: 'block'
        });
    }

    subDataForReg() {
        let canSentForm = true;
        this.setState({
            erLogin: 'errorLoginNone',
            erPassword: 'errorPasswordNone',
            erSurname: 'errorSurnameNone',
            erName: 'errorNameNone',
            erEmail: 'errorEmailNone',
            erPhone: 'errorPhoneNone',
        });

        if (this.state.login.length < 5) {
            canSentForm = false;
            this.setState({
                errorLogin: 'blabla',
                displayLogin: 'block',
                erLogin: 'errorLogin',
            });
        }
        if (this.state.password.length < 8) {
            canSentForm = false;
            this.setState({
                erPassword: 'errorPassword',
            });
        }
        if (this.state.surname.length < 4) {
            canSentForm = false;
            this.setState({
                erSurname: 'errorSurname'
            });
        }
        if (this.state.nameTest.length < 4) {
            canSentForm = false;
            this.setState({
                erName: 'errorName'
            });
        }
        /*  if(this.state.email.length < 8 ) {
              canSentForm = false;
              this.setState({
                  erEmail: 'errorEmail'
              });
          }*/
        if (this.state.phone.length < 8) {
            canSentForm = false;
            this.setState({
                erPhone: 'errorPhone',
            });
        }
        if (canSentForm) {
            axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/buyerreg?login=' + this.state.login + '&password=' + this.state.password + '&surname=' + this.state.surname + '&name=' + this.state.nameTest + '&email=' + this.state.email + '&phone=' + this.state.phone).then(res => {
                console.log(res.data);
                if (res.data.auto_auth) {
                    this.setState({
                        loginAuth: this.state.login,
                        passwordAuth: this.state.password
                    });
                    this.subDataForAuth();
                    this.CloseModalRegister();
                }
            });
            //console.log(this.state.login);
        }
    }

    subDataForAuth() {
        let canSentForm = true;

        if (canSentForm) {
            axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/buyerlogin?login=' + this.state.loginAuth + '&password=' + this.state.passwordAuth).then(res => {
                console.log(res.data);
                let registered = res.data.registered;
                if (registered === undefined) {
                    this.setState({
                            erAuth: "erAuth"
                        }
                    )
                } else {
                    this.setState({
                        showBlockNone: !this.state.showBlockNone,
                        NameUser: "вы вошли как " + res.data.name
                    });

                    this.setState({
                        nameTest: res.data.name,
                        surname: res.data.surname,
                        email: res.data.email,
                        userID: res.data.id,
                        phone: res.data.phone
                    })

                    this.sessionUser();
                    this.CloseModalLogin();
                }
            });
        }
        ;
        // this.sessionUser();

    }

    sessionUser() {
        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/sessionuser?surname=' + this.state.surname + '&name=' + this.state.nameTest + '&email=' + this.state.email + '&phone=' + this.state.phone + '&id=' + this.state.userID).then(res => {

            // console.log(res.data);
            localStorage.name = res.data.name;
            localStorage.surname = res.data.surname;
            localStorage.email = res.data.email;
            localStorage.phone = res.data.phone;
            localStorage.userID = res.data.id;

            this.setState({
                nameS: localStorage.name,
                surnameS: localStorage.surname,
                emailS: localStorage.email,
                userID: localStorage.userID,
                phoneS: localStorage.phone
            });
            this.setState({
                NameUser: res.data.name
            });
            this.setState({
                sessionUserActiv: res.data
            });


        });
    }

    dataSessionUser() {
        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/sessionuserdata').then(res => {

            console.log(res.data);
            this.setState({
                sessionUserActiv: res.data.name
            });

        });

    }

    signFunction() {
        //alert('hello world');
        this.setState({
            showSign: !this.state.showSign,
            showReg: false
        })
    }

    regFunction() {
        this.setState({
            showSign: false,
            showReg: !this.state.showReg
        })
    }

    get UNSAFE_componentWillMount() {
        const root = document.getElementById('root');
        const from_date = root.getAttribute('data-from');
        const to_date = root.getAttribute('data-to');
        if (from_date != '' && to_date != '') {
            axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/getCityNameByCityID?cityID=' + from_date).then(res => {
                this.props.mainAction.setFrom(res.data);
                // this.setState({
                //     from: res.data
                // });
            });
            axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/getCityNameByCityID?cityID=' + to_date).then(res => {
                this.props.mainAction.setTo(res.data);
                // this.setState({
                //     to: res.data
                // });
            });
        }
    }

    componentDidMount() {
        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/getCityList').then(res => {
            this.props.mainAction.setCityList(res.data);
            // this.setState({
            //     cityList: res.data
            // });
        });

        if (localStorage.name) {
            this.setState({nameS: localStorage.name});
        }
        if (localStorage.surname) {
            this.setState({surnameS: localStorage.surname});
        }
        if (localStorage.email) {
            this.setState({emailS: localStorage.email});
        }
        if (localStorage.phone) {
            this.setState({phoneS: localStorage.phone});
        }
        fetch('https://jsonplaceholder.typicode.com/todos/1')
            .then(response => response.json())
            .then(json => {
                this.setState({loading: false});
            })
            .catch(err => {
                this.setState({loading: true});
            });
    }

    handleChangeDateArrow(date) {
        let newDate = new Date();
        newDate.setDate(this.props.on.getDate() + date);
        this.props.mainAction.setOn(newDate);
        this.handleSubmit(newDate);
        // this.setState({
        //     on: newDate
        // }, function () {
        //     this.handleSubmit(this.props.on);
        // });
    }

    setNextDate() {
        let newDate = this.props.nextDate;
        //newDate.setDate(this.state.on.getDate() + date);
        this.props.mainAction.setOn(newDate);
        this.handleSubmit(newDate);
        // this.setState({
        //     on: newDate
        // }, function () {
        //     this.handleSubmit(this.state.on);
        // });
    }

    handleChange(e) {
        this.setState({[e.target.name]: e.target.value});

    }

    handleCheck() {
        this.setState({checkboxOffer: !this.state.checkboxOffer});
    }

    handleOffer() {
        this.setState({offerShow: !this.state.offerShow});
    }

    handleSubmit(el) {
        // console.log(el);
        //this.loadin();
        this.props.mainAction.setLoadingTwo(true);
        // this.setState({
        //     loadingTwo: true,
        // })
        let direct = this.props.direct ? this.props.direct : 0;
        let goback = this.props.goback ? this.props.goback : 0;
        let on_date = this.props.on;
        if (typeof el === 'string' || el instanceof String) {
            on_date = el;
        }

        let day = on_date.getDate() > 9 ? on_date.getDate() : '0' + on_date.getDate();
        let month = on_date.getMonth() + 1;
        let when = on_date.getFullYear() + '-' + month + '-' + day;

        /**
         * when back parse date
         * */
        let onback_date = this.props.onback;
        let dayback = onback_date.getDate() > 9 ? onback_date.getDate() : '0' + onback_date.getDate();
        let monthback = onback_date.getMonth() + 1;
        let whenback = onback_date.getFullYear() + '-' + monthback + '-' + dayback;

        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/getTrips?direct=' + direct + '&fromID=' + this.props.from + '&toID=' + this.props.to + '&on=' + when + '&passengers=' + this.props.passengers + '&goback=' + goback + '&whenback=' + whenback + '&currency=' + this.props.currency).then(res => {
            console.log(res.data);
            this.props.mainAction.setBlockShow('block');
            // this.setState({blockShow: 'block'});
            if (!res.data.error) {
                this.props.mainAction.setLoadingTwo(false);
                // this.setState({
                //     loadingTwo: false
                // });
                this.props.mainAction.setSortMenu(true);
                // this.setState({
                //     sortMenu: true
                // });
                this.props.mainAction.setVisibleCalendar('block');
                // this.setState({visibleCalendar: 'block'});
                this.props.mainAction.setTripList(res.data);
                this.props.mainAction.setTripListTwo(res.data);
                this.props.mainAction.setTripId(res.data[0].tripId);
                // this.setState({
                // tripList: res.data,
                // tripListTwo: res.data,
                // tripId: res.data[0].tripId,
                // });
                let dataCalendarTest = res.data[0].dtArr;
                console.log(dataCalendarTest);
                //   let arrActive = res.data[0].trip_stop.forward[0].depDates;
                let arrDate = [];
                //все даты
                let allDay = [];
                let allDate = [];
                //даты активные
                // let activeDay = [];
                let funcDate = function (dataTwoTest) {
                    for (let i = 0; i < 7; i++) {

                        let result = new Date(dataTwoTest);
                        arrDate.push(new Date(result.getTime() + ((i - 4) * 86400000)));
                    }

                };
                //функция для извлечения дней из массивов
                // let findInArray = function(arrActive, arrDate) {
                //     for (let i=0; i < 7; i++) {
                //         let arrAll = arrDate[i];
                //
                //         let Calendar =  new Date(arrActive[i]);
                //         let dayCall = Calendar.getDay();
                //
                //         arrAll.getDay();
                //
                //         allDay.push(dayCall);
                //
                //     }
                // };
                funcDate(dataCalendarTest);
                //findInArray(arrActive, arrDate);
                let arrDate2 = [];
                for (let i = 0; i < arrDate.length; i++) {
                    let stylingDateElement = {};
                    if (new Date(arrDate[i]) < new Date()) {
                        stylingDateElement = {'color': 'grey', 'cursor': 'no-drop'};
                    }
                    if (new Date(arrDate[i]).getDate() == new Date(this.props.on).getDate()) {
                        stylingDateElement = {
                            'border': '1px solid #cbcbcb',
                            'backgroundColor': '#fff',
                            'fontWeight': '500'
                        };
                    }
                    arrDate2.push({
                        'element': arrDate[i],
                        'stylingDateElement': stylingDateElement,
                        'classDateElement': 'choosen'
                    });
                }
                this.props.mainAction.setBlockShow('block');
                this.props.mainAction.setDataCalendar(arrDate2);
                this.props.mainAction.setResErrorRace(false);
                this.props.mainAction.setShowByTic('none');

                this.setState({
                    // dataCalendar: arrDate2,
                    // resErrorRace: false,
                    // showByTic: 'none',
                    // blockShow: 'block'
                });


            } else {

                axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/getTripsInfo?direct=' + direct + '&fromID=' + this.props.from + '&toID=' + this.props.to + '&on=' + when + '&passengers=' + this.props.passengers + '&goback=' + goback + '&whenback=' + whenback + '&currency=' + this.props.currency).then(res => {
                    if (res.data.error && res.data.error.code == 'E_NODATA') {
                        this.props.mainAction.setBlockShow('block');
                        this.props.mainAction.setShowByTic('none');
                        this.props.mainAction.setResErrorRace2(res.data.error.name);
                        // this.setState({
                        //     resErrorRace2: res.data.error.name,
                        // showByTic: 'none',
                        // blockShow: 'block'
                        // });
                        this.props.mainAction.setLoadingTwo(false);
                        // this.setState({
                        //     loadingTwo: false
                        // });
                    } else {
                        this.props.mainAction.setBlockShow('block');
                        this.props.mainAction.setResErrorRace(true);
                        this.props.mainAction.setShowByTic('none');
                        this.props.mainAction.setNextDate(new Date(res.data[0].depDates[0]));

                        // this.setState({
                        // resErrorRace: true,
                        // nextDate: new Date(res.data[0].depDates[0]),
                        // showByTic: 'none',
                        // blockShow: 'block'
                        // });
                        this.props.mainAction.setLoadingTwo(false);
                        // this.setState({
                        //     loadingTwo: false
                        // });
                    }

                });

            }
        });


    }

    loadin() {
        this.props.mainAction.setLoadingTwo(true);
        // this.setState({
        //     loadingTwo: true
        // })
        setTimeout(
            function () {
                this.props.mainAction.setLoadingTwo(false);
                // this.setState({loadingTwo: false});
            }
                .bind(this),
            4500
        );

    }

    changeDateOnCalendar(el) {
        let nowDate = new Date();
        let clickedDate = new Date(el);
        if (clickedDate >= nowDate) {
            this.props.mainAction.setOn(el);
            this.handleSubmit(el);
            // this.setState({on: el}, function () {
            //     this.handleSubmit(this.state.on);
            // });
        }

    }

    displayCalendar() {
        this.setState({displayCalendar: 'block'});
    }

    infoBlock(e, id) {
        let _InviteList = this.props.tripList;
        _InviteList[id].showDateil = !_InviteList[id].showDateil;
        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/stopTrip?tripId=' + _InviteList[id].tripId).then(res => {
            _InviteList[id].trip_stop = res.data;
            this.props.mainAction.setTripList(_InviteList);
            this.setState({displayCalendar: 'none'});
            // this.setState({tripList: _InviteList});
        });

    };

    infoBlockBack(e, id) {
        let _InviteList = this.props.tripList;
        _InviteList[id].showDateil2 = !_InviteList[id].showDateil2;
        axios.get('http://new.viabona.com.ua/api/index.php/api/octobus/stopTrip?tripId=' + _InviteList[id].backTripId).then(res => {
            _InviteList[id].trip_stop_back = res.data;
            this.props.mainAction.setTripList(_InviteList);
            this.setState({displayCalendar: 'none'});

            // this.setState({tripList: _InviteList});
        });
        //this.setState({tripList : _InviteList});

    };

    render() {
        const autoCompleteStyle = {
            padding: '26px 30px 10px 12px',
            backgroundColor: 'transparent',
            textOverflow: 'ellipsis',
            fontSize: '16px',
            outline: 'none',
            position: 'static',
            overflowY: 'scroll',
            maxHeight: '50vh',
            border: 'none'
        };
        const autoCompleteWrapperStyle = {
            textTransform: 'none',
            padding: '5px 11px',
            color: '#939393',
            fontSize: '17px',
            height: '300px',
            overflowY: 'scroll',
            lineHeight: '30px',
            backgroundColor: '#eee',
            borderBottomLeftRadius: '5px',
            borderBottomRightRadius: '5px',
            position: 'static',
            maxHeight: '50vh'
        };
        const pushme = [];
        let arrayForSession = this.state.sessionUserActiv;


        return (


            <div>
                <div className="mainMainBlock">
                    <div className="myCab">
                        {localStorage.name ?
                            <div>
                                <nav>
                                    <ul className="topMenu">
                                        <li>
                                            <p className="subMenuLink" style={{cursor: 'pointer'}}>{localStorage.name}<p
                                                className="burger">≡</p></p>
                                            <ul className="subMenu">
                                                <li><p id="myProfileLink" className='textCab'
                                                       style={{cursor: 'pointer'}}
                                                       onClick={() => this.showBlockProfileFunction()}>Мой профиль</p>
                                                </li>
                                                <li><p id="paymentTicketsLink" className='textCab'
                                                       style={{cursor: 'pointer'}}
                                                       onClick={() => this.showBlockTicketsFunction()}>Оплаченные
                                                    билеты</p></li>
                                                <li><p id="orderTicketsLink" className='textCab'
                                                       style={{cursor: 'pointer'}}
                                                       onClick={() => this.showBlockTicketsFunctionTwo()}>Забронированные
                                                    билеты</p></li>
                                                <li><p id="exitLink" className='textCab' style={{cursor: 'pointer'}}
                                                       onClick={() => this.deleteLocal()}>Выход</p></li>
                                            </ul>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                            :
                            <div>
                                <a href="#" id="enterLink" style={{cursor: 'pointer'}}
                                   onClick={() => this.showBlockLogin()}>Вход</a> | <a href="#" style={{
                                cursor: 'pointer',
                                color: '#D50707'
                            }} id="registerLink" onClick={() => this.showBlockRegister()}>Регистрация</a>
                            </div>}
                    </div>

                    <div className={this.state.modalUserProfileClass} id="modalUserProfile" tabIndex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true"
                         style={{display: this.state.modalUserProfileStyle}}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" onClick={() => this.CloseModalUserProfile()}
                                            data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 className="modal-title" id="myModalLabel">Мой профиль</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label htmlFor="cp_nameS">Имя:</label>
                                        <input type="text" className="form-control" id="cp_nameS" name="nameS"
                                               value={this.state.nameS} onChange={this.nameS}/>
                                        <label htmlFor="cp_surnameS">Фамилия:</label>
                                        <input type="text" id="cp_surnameS" className="form-control" name="surnameS"
                                               value={this.state.surnameS} onChange={this.surnameS}/>
                                        <label htmlFor="cp_emailS">Почта:</label>
                                        <input type="text" id="cp_emailS" className="form-control" name="emailS"
                                               value={this.state.emailS} onChange={this.emailS}/>
                                        <label htmlFor="cp_phoneS">Телефон:</label>
                                        <input type="text" id="cp_phoneS" className="form-control" name="phoneS"
                                               value={this.state.phoneS} onChange={this.phoneS}/>

                                    </div>

                                    <div className="form-group" style={{textAlign: 'right'}}>
                                        <button className="btn btn-primary verdad-btn"
                                                onClick={this.handleSaveProfile}>Сохранить
                                        </button>
                                    </div>

                                </div>
                                <div className="modal-footer">
                                    <h5 style={{textAlign: 'left'}}>Изменить пароль</h5>
                                    <div className="form-group" style={{textAlign: 'left'}}>
                                        <label htmlFor="cp_passwordS">Новый Пароль:</label>
                                        <input type="text" className="form-control" id="cp_passwordS" name="passwordS"
                                               value={this.state.passwordS} onChange={this.passwordS}/>
                                        <label htmlFor="cp_password2S">Повторите Пароль:</label>
                                        <input type="text" className="form-control" id="cp_password2S" name="password2S"
                                               value={this.state.password2S} onChange={this.password2S}/>
                                    </div>
                                    <button className="btn btn-primary verdad-btn"
                                            onClick={this.handleSaveProfile.bind(this, true)}>Изменить пароль
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>


                    {/*Список билетов*/}
                    <div className={this.state.modalUserTicketsClass} id="modalUserProfile" tabIndex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true"
                         style={{display: this.state.modalUserTicketsStyle}}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" onClick={() => this.CloseModalUserTickets()}
                                            data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 className="modal-title" id="myModalLabel">Мои оплаченные билеты</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="table-responsive"
                                         style={{overflowY: 'scroll', maxHeight: '500px', height: '500px'}}>
                                        <table className="table table-striped table-hovered table-bordered">
                                            <tbody>
                                            {this.state.ticketsInfo.map((list, key) =>
                                                <tr key={key}>
                                                    <td>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px',
                                                                    width: '50%'
                                                                }}><b>Отправление:</b><br/>
                                                                    {list.lastName} {list.name}
                                                                    <br/>
                                                                    {list.timeDep}, {list.dateDep} | {list.stDepName} | {list.stDepAddr}
                                                                </td>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px',
                                                                    width: '50%'
                                                                }}><b>Прибытие:</b><br/>
                                                                    {list.timeArr}, {list.dateArr} | {list.stArrName} | {list.stArrAddr}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px', width: '50%'
                                                                }}>
                                                                    <b>В пути:</b><br/>
                                                                    {list.wayHour}:{list.wayMin}<br/>
                                                                    <b>Место:</b><br/>
                                                                    {list.place}
                                                                </td>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px',
                                                                    width: '50%'
                                                                }}>
                                                                    <b>Цена:</b><br/>
                                                                    {list.price} {list.curr}
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <hr/>
                                                    </td>
                                                </tr>
                                            )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>


                    <div className={this.state.modalUserTicketsClassTwo} id="modalUserProfile" tabIndex="-1"
                         role="dialog" aria-labelledby="myModalLabel" aria-hidden="true"
                         style={{display: this.state.modalUserTicketsStyleTwo}}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close"
                                            onClick={() => this.CloseModalUserTicketsTwo()} data-dismiss="modal"
                                            aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 className="modal-title" id="myModalLabel">Мои забронированные билеты</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="table-responsive"
                                         style={{overflowY: 'scroll', maxHeight: '500px', height: '500px'}}>
                                        <table className="table table-striped table-hovered table-bordered">
                                            <tbody>
                                            {this.state.reserveObjInfo.map((list, key) =>
                                                <tr key={key}>
                                                    <td>
                                                        <table>
                                                            <tbody>
                                                            <tr>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px',
                                                                    width: '50%'
                                                                }}><b>Отправление:</b><br/>
                                                                    {list.pFIO}
                                                                    <br/>
                                                                    {list.dTime} | {list.sFrom} | {list.sFAddr}
                                                                </td>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px',
                                                                    width: '50%'
                                                                }}><b>Прибытие:</b><br/>
                                                                    {list.aTime} | {list.sTo} | {list.sTAddr}
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px', width: '50%'
                                                                }}>
                                                                    <b>Место:</b><br/>
                                                                    {list.place}
                                                                </td>
                                                                <td style={{
                                                                    border: '1px solid #dee2e6',
                                                                    verticalAlign: 'top',
                                                                    textAlign: 'left',
                                                                    padding: '10px',
                                                                    width: '50%'
                                                                }}>
                                                                    <b>Цена:</b><br/>
                                                                    {this.state.mSum} {list.curr}
                                                                    <div className="submit-btn">
                                                                        {
                                                                            this.state.showBuyNextTwo ?
                                                                                <button className="reserveButton"
                                                                                        type="submit" style={{
                                                                                    backgroundColor: 'white',
                                                                                    border: '1px solid black'
                                                                                }}
                                                                                        onClick={() => this.PayArrTwo(this.state.nameS, this.state.surnameS, this.state.emailS, this.state.phoneS)}>
                                                                                    Оплатить
                                                                                </button>

                                                                                :
                                                                                null
                                                                        }


                                                                        {
                                                                            this.state.showButPayTicTwo ?
                                                                                <div
                                                                                    dangerouslySetInnerHTML={{__html: this.state.butPayTicTwo}}/>

                                                                                :
                                                                                null
                                                                        }
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                    <td>
                                                        <hr/>
                                                    </td>
                                                </tr>
                                            )}
                                            </tbody>
                                        </table>
                                    </div>
                                </div>

                                <div className="modal-footer">
                                </div>

                            </div>
                        </div>
                    </div>


                    <div className="blockSign" style={{display: this.state.showBlockTickets ? 'block' : 'none'}}>
                        <div>
                            <div>
                                <div>
                                    <div className="showSignBlock">
                                        <h3>Список билетов</h3>
                                        {this.state.ticketsInfo.map((list, key) =>
                                            <div key={key}>
                                                <p>
                                                    Название рейса: {list.raceName}
                                                </p><p className="mobilePerevozchik">
                                                Перевозчик: {list.carrierName}
                                            </p>
                                            </div>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {/* форма авторизации*/}

                    <div className={this.state.modalUserLoginClass} id="modalUserLogin" tabIndex="-1" role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true"
                         style={{display: this.state.modalUserLoginStyle}}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" onClick={() => this.CloseModalLogin()}
                                            data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 className="modal-title" id="myModalLabel">Авторизация</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label htmlFor="loginAuth">E-mail:</label>
                                        <input className="form-control" id="loginAuth" type="text" name="loginAuth"
                                               value={this.state.loginAuth} onChange={this.handleChange}/>
                                        <label htmlFor="passwordAuth">Пароль:</label>
                                        <input className="form-control" type="password" name="passwordAuth"
                                               value={this.state.passwordAuth} onChange={this.handleChange}/>
                                        <p className={this.state.erAuth}>Логин или пароль введены не верно</p>
                                    </div>

                                    <div className="form-group" style={{textAlign: 'right'}}>
                                        <button className="btn btn-primary verdad-btn"
                                                onClick={this.subDataForAuth}>Войти
                                        </button>
                                    </div>

                                </div>
                                <div className="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>


                    {/* форма регистрации*/}

                    <div className={this.state.modalUserRegisterClass} id="modalUserRegister" tabIndex="-1"
                         role="dialog"
                         aria-labelledby="myModalLabel" aria-hidden="true"
                         style={{display: this.state.modalUserRegisterStyle}}>
                        <div className="modal-dialog" role="document">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" onClick={() => this.CloseModalRegister()}
                                            data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                    <h4 className="modal-title" id="myModalLabel">Регистрация</h4>
                                </div>
                                <div className="modal-body">
                                    <div className="form-group">
                                        <label htmlFor="login">E-mail:</label>
                                        <input placeholder="email@gmail.com" id="login" type="text"
                                               className="form-control {this.state.errorLogin}" name="login"
                                               value={this.state.login} onChange={this.handleChange}/>
                                        <p className={this.state.erLogin}>не коректно введен логин </p>

                                        <label htmlFor="password">Пароль:</label>
                                        <input placeholder="Пароль" id="password"
                                               className="form-control {this.state.erPassword}" type="password"
                                               name="password" value={this.state.password}
                                               onChange={this.handleChange}/>
                                        <p className={this.state.erPassword}>не коректный пароль</p>


                                        <label htmlFor="email" style={{display: 'none'}}>Email:</label>
                                        <input style={{display: 'none'}} placeholder="email@gmail.com"
                                               className="form-control {this.state.erEmail}" type="text" name="email"
                                               value={this.state.email} onChange={this.handleChange}/>
                                        <p style={{display: 'none'}} className={this.state.erEmail}>Введите емеил
                                            коректно</p>


                                        <label htmlFor="nameTest">Имя:</label>
                                        <input placeholder="Имя" id="nameTest"
                                               className="form-control {this.state.erName}" type="text" name="nameTest"
                                               value={this.state.nameTest}
                                               onChange={this.handleChange}/>
                                        <p className={this.state.erName}>Введите имя коректно</p>


                                        <label htmlFor="surname">Фамилия:</label>
                                        <input id="surname" placeholder="Ваша фамилия"
                                               className="form-control {this.state.erSurname}" type="text"
                                               name="surname" value={this.state.surname} onChange={this.handleChange}/>
                                        <p className={this.state.erSurname}>Фамилия введена не корректно</p>


                                        <label htmlFor="phone">Телефон:</label>
                                        <input id="phone" placeholder="+38 099-000-00-00"
                                               className="form-control {this.state.erPhone}" type="text" name="phone"
                                               value={this.state.phone} onChange={this.handleChange}/>
                                        <p className={this.state.erPhone}>Введите мобильный номер корректно</p>

                                    </div>

                                    <div className="form-group" style={{textAlign: 'right'}}>
                                        <button className="btn btn-primary verdad-btn"
                                                onClick={this.subDataForReg}>Зарегистрироваться
                                        </button>
                                    </div>

                                </div>
                                <div className="modal-footer">

                                </div>
                            </div>
                        </div>
                    </div>
                    {/*    конец блока регистрации*/}


                    <div className="fixBox">
                        <div>{
                            this.props.loadingTwo ?
                                <div className="preloader_wrapper">
                                    <div className="preloader_item">

                                        <div className="preloader_item_top">
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_right">
                                                <div className="preloader_item_top_right_1"></div>
                                                <div className="preloader_item_top_right_2"></div>
                                            </div>
                                        </div>
                                        <div className="preloader_item_bottom">
                                            <div className="preloader_item_bottom_1"></div>
                                        </div>
                                    </div>
                                    <div className="preloader_item">
                                        <div className="preloader_item_top">
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_right">
                                                <div className="preloader_item_top_right_1"></div>
                                                <div className="preloader_item_top_right_2"></div>
                                            </div>
                                        </div>
                                        <div className="preloader_item_bottom">
                                            <div className="preloader_item_bottom_1"></div>
                                        </div>
                                    </div>
                                    <div className="preloader_item">
                                        <div className="preloader_item_top">
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_right">
                                                <div className="preloader_item_top_right_1"></div>
                                                <div className="preloader_item_top_right_2"></div>
                                            </div>
                                        </div>
                                        <div className="preloader_item_bottom">
                                            <div className="preloader_item_bottom_1"></div>
                                        </div>
                                    </div>
                                    <div className="preloader_item">
                                        <div className="preloader_item_top">
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_left">
                                                <div className="preloader_item_top_left_1"></div>
                                                <div className="preloader_item_top_left_2"></div>
                                                <div className="preloader_item_top_left_3"></div>
                                                <div className="preloader_item_top_left_4"></div>
                                            </div>
                                            <div className="preloader_item_top_right">
                                                <div className="preloader_item_top_right_1"></div>
                                                <div className="preloader_item_top_right_2"></div>
                                            </div>
                                        </div>
                                        <div className="preloader_item_bottom">
                                            <div className="preloader_item_bottom_1"></div>
                                        </div>
                                    </div>
                                </div>
                                : null
                        }
                        </div>

                        {/* <div>{
                    //     this.state.loadingTwo?
                    //         <div className="mainImgLoad">
                    //             <div className="imgLoad"></div>
                    //         </div>
                    //         : null
                    // }
                    // </div> */}


                    </div>

                    <div style={{display: this.props.blockShow}}>
                        <div className='mainBlockError'>
                            {this.props.resErrorRace ?
                                <p className="errorSend"> К сожалению по вашему запросу не найдено рейсов в назначеную
                                    дату<br/>Следующий ближайший рейс от выбранной даты <a href="#"
                                                                                           onClick={this.setNextDate}
                                                                                           style={{cursor: 'pointer'}}>
                                        <Moment format="D MMMM" withTitle>
                                            {this.props.nextDate}
                                        </Moment></a></p>
                                :
                                null
                            }
                            {this.props.resErrorRace2 ?
                                <p className="errorSend"> {this.props.resErrorRace2} </p>
                                :
                                null
                            }
                        </div>
                        <div className="blockCalender" style={{display: this.props.blockShow, margin: '0 auto'}}>
                            <div className="calendar__prev"
                                 style={{display: this.props.visibleCalendar}}
                                 onClick={this.handleChangeDateArrow.bind(this, -1)}>
                            </div>
                            {/*календарь*/}
                            {this.props.dataCalendar.map((dateCalendar, key) =>
                                <div key={key} onClick={() => this.changeDateOnCalendar(dateCalendar.element)}>
                                    <div className="calendar__item calendar__key3 styleForDays"
                                         style={dateCalendar.stylingDateElement}>
                                        <p>
                                            <Moment format="D MMM" withTitle>
                                                {dateCalendar.element}
                                            </Moment><br/>
                                            <Moment format="dddd" withTitle>
                                                {dateCalendar.element}
                                            </Moment>
                                        </p></div>
                                </div>
                            )}
                            <div className="calendar__next"
                                 style={{display: this.props.visibleCalendar}}
                                 onClick={this.handleChangeDateArrow.bind(this, 1)}>
                            </div>
                        </div>
                        <div className="fixCalendar" style={{display: this.props.blockShow}}>

                        </div>


                        <div className="sort" style={{display: this.props.sortMenu ? 'flex' : 'none'}}>
                            <div className="sortItem active" onClick={this.sortSortDep}>Время отправления <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 320 512"
                                className="svg-is-active">
                                <path fill="currentColor"
                                      d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z"></path>
                            </svg></div>
                            <div className="sortItem" onClick={this.sortSortWay}>Время в пути <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 320 512"
                                className="svg-is-active">
                                <path fill="currentColor"
                                      d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z"></path>
                            </svg></div>
                            <div className="sortItem" onClick={this.sortSortArr}>Время прибытия <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 320 512"
                                className="svg-is-active">
                                <path fill="currentColor"
                                      d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z"></path>
                            </svg></div>
                            <div className="sortItem" onClick={this.sortSortPrice}>Стоимость <svg
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 320 512"
                                className="svg-is-active">
                                <path fill="currentColor"
                                      d="M41 288h238c21.4 0 32.1 25.9 17 41L177 448c-9.4 9.4-24.6 9.4-33.9 0L24 329c-15.1-15.1-4.4-41 17-41z"></path>
                            </svg></div>
                        </div>

                        <div className="blockShow_wrap">
                            {this.props.tripList.map((list, key) =>
                                <div key={key} className="blockShow">
                                    {/*//новый блок для вывода*/}
                                    <div className="showBlock" style={{display: this.props.blockShow}}
                                         data-sort-departure={list.dtDepSec} data-sort-arrive={list.dtArrSec}
                                         data-sort-waytime={list.wayTimeSec} data-sort-price={Math.trunc(list.price)}>
                                        <div className='upShowBlock'>
                                            <div className='oneGridForShow'>
                                                <div className='upOneGridForShow'>
                                                    {console.error('list.dtDep --> ', list.dtDep)}
                                                    <p className="timeShow">
                                                        {/*<Moment format="HH:MM" withTitle>*/}
                                                        {moment(list.dtDep).locale("uk").format("LT")}
                                                        {/*{moment(list.dtDep).locale('uk').format('hh:mm')}*/}
                                                        {/*</Moment>*/}
                                                    </p>
                                                    <div className="dataShow">
                                                        {moment(list.dtDep).format('D MMM')}
                                                        {/*    <Moment format="D MMM" withTitle>*/}
                                                        {/*    {list.dtDep}*/}
                                                        {/*</Moment>*/}
                                                    </div>
                                                    <div className="timeInRoad">{list.wayTimeH} ч {list.wayTimeM} мин. в
                                                        пути
                                                    </div>
                                                </div>
                                                <div className='upTwoGridForShow'>
                                                    <p className="cityName">{list.stDepName}</p>
                                                    <p>{list.stDepAddr}</p>
                                                </div>
                                            </div>
                                            <div className='twoGridForShow'>
                                                <div className='upOneGridForShow'>
                                                    <p className="timeShow">
                                                        {/*<Moment format="HH:MM" withTitle>*/}
                                                        {/*    {list.dtArr}*/}
                                                            {moment(list.dtArr).locale("uk").format("LT")}
                                                        {/*</Moment>*/}
                                                    </p>
                                                    <div className="dataShow"><Moment format="D MMM" withTitle>
                                                        {list.dtArr}
                                                    </Moment></div>
                                                </div>
                                                <div className='upTwoGridForShow'>
                                                    <p className="cityName">{list.stArrName}</p>
                                                    <p>{list.stArrAddr}</p>
                                                </div>
                                            </div>
                                            <div className='threeGridForShow'>
                                                <div className='leftShowBlock'>
                                                    <p className='textPrice'
                                                       style={{float: 'right'}}>{list.price} {list.currName}</p>
                                                </div>
                                                <div className='rightShowBlock'>
                                                    {!this.props.goback ? <button className="butTic"
                                                                                  onClick={() => this.showByTic(list.price, list.currency, list.racename, list.dtArr, list.stArrName, list.stDepName, list.dtDep, list.stDepAddr, list.stArrAddr, this.props.passengers, list.currName, list.id, list.tripId, key)}>Выбрать</button> : ''}
                                                    <br/>
                                                    <p className='textPass'
                                                       style={{color: '#D50707'}}>{list.places} мест</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className='botShowBlock'>
                                            <div className=""><p className="trip__details__down"
                                                                 onClick={(e) => this.infoBlock(e, key)}
                                                                 style={{color: '#D50707'}}>Детали
                                                рейса</p></div>
                                            <div className="mobilePerevozchik">
                                                <div className="mobilePerevozchik_one">
                                                    <span>Перевозчик:</span> {list.carrier}
                                                </div>
                                                <div className="mobilePerevozchik_two">
                                                    <span>Рейс:</span> {list.racename}
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <Racedetailblock list={list}/>
                                    {/*end new show*/}
                                    {this.props.goback ?
                                        <div>
                                            <div className="showBlock"
                                                 style={{display: this.props.blockShow, height: '233px'}}>
                                                <hr style={{borderTop: '1px solid red'}}/>
                                                <h4 style={{textAlign: 'center'}}>Обратный билет</h4>
                                                <div className='upShowBlock'>
                                                    <div className='oneGridForShow'>
                                                        <div className='upOneGridForShow'>
                                                            <p className="timeShow">
                                                                {moment(list.backDtDep).locale("uk").format("LT")}
                                                                {/*<Moment format="HH:MM" withTitle>*/}
                                                                {/*    {list.backDtDep}*/}
                                                                {/*</Moment>*/}
                                                            </p>
                                                            <div className="dataShow"><Moment format="D MMM" withTitle>
                                                                {list.backDtDep}
                                                            </Moment></div>
                                                            <div
                                                                className="timeInRoad">{list.backWayTimeH} ч {list.backWayTimeM} мин.
                                                                в пути
                                                            </div>
                                                        </div>
                                                        <div className='upTwoGridForShow'>
                                                            <p className="cityName">{list.backStDepName}</p>
                                                            <p>{list.backStDepAddr}</p>
                                                        </div>
                                                    </div>
                                                    <div className='twoGridForShow'>
                                                        <div className='upOneGridForShow'>
                                                            <p className="timeShow">
                                                                {moment(list.backDtArr).locale("uk").format("LT")}
                                                                {/*<Moment format="HH:MM" withTitle>*/}
                                                                {/*    {list.backDtArr}*/}
                                                                {/*</Moment>*/}
                                                            </p>
                                                            <div className="dataShow"><Moment format="D MMM" withTitle>
                                                                {list.backDtArr}
                                                            </Moment></div>
                                                        </div>
                                                        <div className='upTwoGridForShow'>
                                                            <p className="cityName">{list.backStArrName}</p>
                                                            <p>{list.backStArrAddr}</p>
                                                        </div>
                                                    </div>
                                                    <div className='threeGridForShow'>
                                                        <div className='leftShowBlock'>
                                                            <p className='textPrice'
                                                               style={{float: 'right'}}>{list.backPrice} {list.currName}</p>
                                                        </div>
                                                        <div className='rightShowBlock'>
                                                            <button className="butTic"
                                                                    onClick={() => this.showByTic(list.price, list.currency, list.racename, list.dtArr, list.stArrName, list.stDepName, list.dtDep, list.stDepAddr, list.stArrAddr, this.props.passengers, list.currName, list.id, list.tripId, key)}>Выбрать
                                                            </button>
                                                            <br/>
                                                            <p className='textPass'
                                                               style={{color: '#D50707'}}>{list.backPlaces} мест</p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className='botShowBlock'>
                                                    <div className=""><p className="trip__details__down"
                                                                         onClick={(e) => this.infoBlockBack(e, key)}
                                                                         style={{color: '#D50707'}}>Детали рейса</p>
                                                    </div>
                                                    <p style={{paddingBottom: "10px"}}
                                                       className="mobilePerevozchik">Перевозчик: {list.backCarrier}<br/>Рейс: {list.backRacename}
                                                    </p>
                                                </div>

                                            </div>
                                            <Racedetailblockback list={list}/></div>
                                        : null}
                                </div>
                            )}
                        </div>

                    </div>
                    {/*блок авторизации и входа линый кабинет*/}


                    <div className="topFix">
                        <button style={{display: this.props.showByTic}} onClick={this.backStup}>Назад</button>

                    </div>


                    <div className="cabin" style={{display: this.props.showByTic}}>


                        <div className="checkout">

                            {this.state.arrTicTwo.map((list, key) =>
                                <div key={key} className="">

                                    {this.state.showDetails ?
                                        <div className="details"
                                             style={{display: this.state.showDetails ? 'block' : 'none'}}>
                                            <div className="aboutDetails"><h5>О рейсе</h5></div>
                                            <div className="aboutDetails_items_list">
                                                <div className="aboutDetails_item">
                                                    <div className="aboutDetails_time">
                                                        <p className="detailsHours">
                                                            {moment(list.dtDep).locale("uk").format("LT")}
                                                            {/*<Moment format="HH:MM" withTitle>*/}
                                                            {/*    {list.dtDep}*/}
                                                            {/*</Moment>*/}
                                                        </p>
                                                        <p className="detailsMonth">
                                                            <Moment format="D MMM" withTitle>
                                                                {list.dtDep}
                                                            </Moment>
                                                        </p>
                                                    </div>
                                                    <div className="aboutDetails_line">
                                                        <div className="aboutDetails_line_circle"></div>
                                                        <div className="aboutDetails_line_ver"></div>
                                                    </div>
                                                    <div className="aboutDetails_place">
                                                        <p className="detailsFromP">{list.stDepName}</p>
                                                        <p className="detailsFromP">{list.stDepAddr}</p>
                                                    </div>
                                                </div>
                                                <div className="aboutDetails_item">
                                                    <div className="aboutDetails_time">
                                                        <p className="detailsHours">
                                                            {moment(list.dtArr).locale("uk").format("LT")}

                                                            {/*<Moment format="HH:MM" withTitle>*/}
                                                            {/*    {list.dtArr}*/}
                                                            {/*</Moment>*/}
                                                        </p>
                                                        <p className="detailsMonth">
                                                            <Moment format="D MMM" withTitle>
                                                                {list.dtArr}
                                                            </Moment>
                                                        </p>
                                                    </div>
                                                    <div className="aboutDetails_line">
                                                        <div className="aboutDetails_line_circle"></div>
                                                        <div className="aboutDetails_line_ver"></div>
                                                    </div>
                                                    <div className="aboutDetails_place">
                                                        <p className="detailsFromP">{list.stArrName}</p>
                                                        <p className="detailsFromP">{list.stArrAddr}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="priceDetail">
                                                <div className="priceDetailText">
                                                    <div className="priceDetailTextT">К оплате:</div>
                                                    <div className="priceDetailPrice">
                                                        {this.state.priceBy} {this.state.currNameBy}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        : null}
                                </div>
                            )}
                            <div className="m-verify-panel__item checkout-panel">
                                <div className="m-verify-panel__item-row">
                                    <div><h5 className="m-t-0 m-b-0 h4">Оформление билета</h5>
                                    </div>
                                </div>
                                <div className="row-custom">
                                    <div className="col-md-12">
                                        <div className="">
                                            <div className="col-md-6 col-sm-6 col-xs-12"><label
                                                className="m-verify-panel__form-label"
                                                htmlFor="checkout_passenger1_1021051141151169511097109101">Имя</label>
                                                <div className={this.state.nameSBlockClass}>
                                                    <div className="">
                                                        <div>
                                                            <div>
                                                                <input
                                                                    name="nameS"
                                                                    type="text" className="form-control"
                                                                    placeholder="Иван"
                                                                    label="Имя" value={this.state.nameS}
                                                                    onChange={this.nameS}/>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <label style={{display: this.state.nameSErrorBlock}}
                                                           className="error control-label"
                                                           htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                        ваше имя</label></div>
                                            </div>
                                            <div className="col-md-6 col-sm-6 col-xs-12"><label
                                                className="m-verify-panel__form-label"
                                                htmlFor="checkout_passenger1_108971151169511097109101">Фамилия</label>
                                                <div className={this.state.surnameSBlockClass}>
                                                    <div className="">
                                                        <div>
                                                            <div><input
                                                                id="checkout_passenger1_108971151169511097109101"
                                                                type="text" className="form-control"
                                                                placeholder="Иванов" autoCapitalize="true"
                                                                value={this.state.surnameS} onChange={this.surnameS}
                                                                autoCorrect="off" label="Фамилия"
                                                                autoComplete="xRfb0tATz" name="xRfb0tATz_"/>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <label style={{display: this.state.surnameSErrorBlock}}
                                                           className="error control-label"
                                                           htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                        вашу фамилию</label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className=""></div>
                                <div className="checkout-seatmap__wrapper"><p>Место в автобусе</p>
                                    <div className="checkout-seatmap checkout__seatmap">
                                        <div className="checkout-seatmap__item">
                                            <div className="">
                                                <div>
                                                    <div className="verify-panel__picker">
                                                        {this.state.matrix[0] ?
                                                            <div
                                                                className={'verify-panel__picker-btn enabled ' + this.state.seatBlockClass}>
                                                                <button className="btn free"
                                                                        onClick={this.showSeats.bind(this, this.state.matrix)}>
                                                                    <span><i
                                                                        className="icon icon-seat-v2"></i></span><span
                                                                    className="verify-panel__picker-description">

                                                            <p id={'seatsQuantity'}>  Количество мест: {this.props.passengers} </p>
                                                        </span>
                                                                </button>
                                                                <label style={{display: this.state.seatErrorBlock}}
                                                                       className="error control-label"
                                                                       htmlFor="none">Выберите места!</label>
                                                            </div>
                                                            :
                                                            <div><p className="freeChoice">Свободная рассадка</p></div>
                                                        }
                                                        <div className="thereMatrix" style={{
                                                            display: this.state.showSeatsBlock ? 'grid' : 'none',
                                                            grid: 'repeat(this.state.seatsRows,5px) / repeat(this.state.seatsColumns,5px)'
                                                        }}>
                                                            {this.state.matrix[0] ? this.state.matrix[0].map((el, i) => {
                                                                return (
                                                                    <div key={i}>
                                                                        {el.map((ryad, ii) => {
                                                                            return (
                                                                                <div
                                                                                    className={ryad.free ? 'divWidth' : 'divWidth full'}
                                                                                    onClick={this.selectPlace.bind(this, ryad, i, ii)}
                                                                                    key={ii} style={{
                                                                                    borderRadius: '5px',
                                                                                    textAlign: 'center',
                                                                                    margin: '3px',
                                                                                    width: '28px',
                                                                                    height: '28px',
                                                                                    color: ryad.selected ? '#fff' : '',
                                                                                    cursor: ryad.free ? 'pointer' : 'no-drop',
                                                                                    backgroundColor: ryad.selected ? '#D50707' : ryad.free ? 'white' : ryad.n ? 'gray' : 'white',
                                                                                }}>{ryad.n}</div>
                                                                            )
                                                                        })}
                                                                        <br/>
                                                                    </div>)
                                                            }) : ''}

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <div className="checkout-panel checkout__customer"><p
                                className="checkout__customer-title">Информация о покупателе</p><p
                                className="checkout__customer-text">Указывайте корректные e-mail и номер телефона, т.к.
                                они
                                необходимы для идентификации пользователя, получения билета, возможности авторизации в
                                Личном Кабинете и
                                возможности вернуть билет.</p>
                                <div className="checkout__customer-form">
                                    <div className="col-md-6 col-sm-6 col-xs-12"><label
                                        className="m-verify-panel__form-label" htmlFor="checkout_email">E-mail</label>
                                        <div className={this.state.emailSBlockClass}><input id="checkout_email"
                                                                                            type="email"
                                                                                            className="form-control"
                                                                                            placeholder="ashevchenko@gmail.com"
                                                                                            value={this.state.emailS}
                                                                                            onChange={this.emailS}
                                                                                            label="E-mail"
                                                                                            autoCorrect="off"
                                                                                            autoCapitalize="false"
                                                                                            autoComplete="on" name=""
                                        />
                                            <label style={{display: this.state.emailSErrorBlock}}
                                                   className="error control-label"
                                                   htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                вашу почту</label>
                                        </div>
                                    </div>
                                    <div className="col-md-6 col-sm-6 col-xs-12"><label
                                        className="m-verify-panel__form-label" htmlFor="checkout_phone">Телефон</label>
                                        <div className={this.state.phoneSBlockClass}>
                                            <div><input id="checkout_phone" name="phone" type="tel" maxLength="17"
                                                        value={this.state.phoneS} onChange={this.phoneS}
                                                        autoComplete="on" label="Телефон" placeholder="380 __ ___ ____"
                                                        className="auth__input"/><span
                                                className="auth__plus text-muted"> </span>
                                            </div>
                                            <label style={{display: this.state.phoneSErrorBlock}}
                                                   className="error control-label"
                                                   htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                ваш телефон</label>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            {this.state.userAr.map((list, key) =>

                                <div key={key} className="checkout-panel checkout__customer"><p
                                    className="checkout__customer-title">Информация о пассажире {list.kkey}</p>
                                    <div className="checkout__customer-form">
                                        <div className="col-md-6 col-sm-6 col-xs-6"><label
                                            className="m-verify-panel__form-label"
                                            htmlFor="checkout_passenger1_1021051141151169511097109101">Имя</label>
                                            <div className={list.blocknameS}>
                                                <div className="">
                                                    <div>
                                                        <div>
                                                            <input
                                                                name="nameS[]"
                                                                type="text" className="form-control" placeholder="Иван"
                                                                label="Имя"
                                                                value={list.nameS}
                                                                onChange={this.pushnameS(key)}/>

                                                        </div>
                                                    </div>
                                                </div>
                                                <label
                                                    style={{'display': list.labelnameS}}
                                                    className="error control-label"
                                                    htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                    ваше имя</label></div>
                                        </div>
                                        <div className="col-md-6 col-sm-6 col-xs-6"><label
                                            className="m-verify-panel__form-label"
                                            htmlFor="checkout_passenger1_108971151169511097109101">Фамилия</label>
                                            <div className={list.blocksurnameS}>
                                                <div className="">
                                                    <div>
                                                        <div><input type="text"
                                                                    className="form-control"
                                                                    placeholder="Иванов"
                                                                    autoCapitalize="true"
                                                                    value={list.surnameS}
                                                                    onChange={this.pushsurnameS(key)}
                                                                    autoCorrect="off"
                                                                    label="Фамилия"
                                                                    name="surnameS[]"/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <label
                                                    style={{'display': list.labelsurnameS}}
                                                    className="error control-label"
                                                    htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                    вашу фамилию</label>
                                            </div>
                                        </div>

                                        <div className="col-md-6 col-sm-6 col-xs-6"><label
                                            className="m-verify-panel__form-label"
                                            htmlFor="checkout_phone">Телефон</label>
                                            <div className={list.blockphoneS}>
                                                <div><input name="phoneS[]"
                                                            type="tel"
                                                            maxLength="17"
                                                            value={list.phoneS}
                                                            onChange={this.pushphoneS(key)}
                                                            autoComplete="on"
                                                            label="Телефон"
                                                            placeholder="380 __ ___ ____"
                                                            className="auth__input"/><span
                                                    className="auth__plus text-muted"> </span>
                                                </div>
                                                <label
                                                    style={{'display': list.labelphoneS}}
                                                    className="error control-label"
                                                    htmlFor="checkout_passenger1_1021051141151169511097109101">Напишите
                                                    ваш телефон</label>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            )}
                            {/* <div className="checkout-panel checkout__customer"><p
                            className="checkout__customer-title">Информация о рейсе</p><p
                            className="checkout__customer-text">
                            Рейс: {this.state.tripInfForBy} <br/>
                            Отправление: {this.state.stDepAddrInfForBy}  <br/>
                            {this.state.DepInfForBy} <br/>
                            Отправление: {this.state.stArrNameInfForBy}  <br/>
                            {this.state.ArrInfForBy}
                        </p>


                        </div> */}
                            <div className="checkout-panel checkout__payment">
                                <div className="checkout__payment-header">К
                                    оплате {this.state.priceBy} {this.state.currNameBy} <br/><span
                                        style={{fontSize: '14px', color: '#D50707'}}>{this.state.promoCodeName}</span>
                                </div>
                                <div className="checkout__payment-promo">


                                    <label htmlFor="promoInput">Введите промокод</label>
                                    <input type="text"
                                           name="promoInput"
                                           id="promoInput"
                                           onChange={this.handleChange}
                                           value={this.state.promoInput}
                                    />
                                    <button onClick={this.promocodeRequest}
                                            className="promoButton"
                                            type="submit"
                                            style={{color: 'white', backgroundColor: '#D50707', width: '100px'}}>OK
                                    </button>

                                    <div style={{
                                        color: '#D50707',
                                        display: this.state.showRegOffer ? 'block' : 'none'
                                    }}> Невозможно определить покупателя.Возможно Вы не зарегистирированы в системе,либо
                                        не осуществили вход<br/></div>
                                </div>
                                <div className="checkout__payment-info">
                                    <div className="checkout__payment-info-text"><br/>Ваши платежные и личные данные
                                        надежно
                                        защищены в соответствии с международными стандартами безопасности.<br/><br/>
                                        <div>
                                            <input id="checkboxInput" type="checkbox" onChange={this.handleCheck}
                                                   defaultChecked={this.state.checkboxOffer}></input>
                                            <label className="checkboxLabel" htmlFor="checkboxInput">Я принимаю условия
                                                возврата, публичной <a className="offer"
                                                                       href="http://new.viabona.com.ua/oferta/"
                                                                       target="_blank">оферты</a> и даю согласие на
                                                обработку персональных данных.
                                            </label>
                                        </div>

                                    </div>
                                    <div className="checkout__payment-info-icons">
                                        <div className="filling-info__pay-item filling-info__pay-maestro"></div>
                                        <div className="filling-info__pay-item filling-info__pay-mastercard"></div>
                                        <div className="filling-info__pay-item filling-info__pay-visa"></div>
                                    </div>
                                </div>


                            </div>
                            <div className="checkout-submit-btn">
                                <button disabled={this.state.checkboxOffer ? "" : "disabled"}
                                        onClick={() => this.setReserve(this.state.nameS, this.state.surnameS, this.state.emailS, this.state.phoneS)}
                                        className="reserveButton" type="submit"
                                        style={{
                                            backgroundColor: '#D50707',
                                            color: 'white',
                                            cursor: this.state.checkboxOffer ? "pointer" : "not-allowed"
                                        }}>Забронировать
                                </button>
                                {
                                    this.state.showBuyNext ?
                                        <button disabled={this.state.checkboxOffer ? "" : "disabled"}
                                                className="butTictwo" type="submit" style={{
                                            backgroundColor: '#D50707',
                                            color: 'white',
                                            cursor: this.state.checkboxOffer ? "pointer" : "not-allowed"
                                        }}
                                                onClick={() => this.PayArr(this.state.nameS, this.state.surnameS, this.state.emailS, this.state.phoneS)}>
                                            Купить
                                        </button>

                                        :
                                        null
                                }


                                {
                                    this.state.showButPayTic ?
                                        <div dangerouslySetInnerHTML={{__html: this.state.butPayTic}}/>

                                        :
                                        null
                                }
                            </div>
                            <div className="checkout__security"><p className="checkout__security-text">Ваши платежные и
                                личные данные надежно защищены.</p></div>
                        </div>
                    </div>
                    <div className="offerShow" style={{display: this.state.offerShow ? 'block' : 'none'}}>*/}
                        <div className="col flex">
                            <div>{this.state.offer}</div>
                        </div>
                    </div>
                    {this.state.offerShow ?
                        <Offerblock offer={this.state.offer}/>

                        :
                        null
                    }

                </div>

                <React.Fragment>
                    <Lines customLoading={this.state.loading}/>
                </React.Fragment>
            </div>


        )
    }
}

function mapStateToProps(state) {
    return {
        on: state.main.on,
        cityList: state.main.cityList,
        showFindBlock: state.main.showFindBlock,
        direct: state.main.direct,
        goback: state.main.goback,
        onback: state.main.onback,
        loadingTwo: state.main.loadingTwo,
        from: state.main.from,
        to: state.main.to,
        passengers: state.main.passengers,
        currency: state.main.currency,
        visibleCalendar: state.main.visibleCalendar,
        tripList: state.main.tripList,
        tripId: state.main.tripId,
        dataCalendar: state.main.dataCalendar,
        showByTic: state.main.showByTic,
        blockShow: state.main.blockShow,
        arrTic: state.main.arrTic,
        priceBy: state.main.priceBy,
        currNameBy: state.main.currNameBy,
        tripInfForBy: state.main.tripInfForBy,
        stDepAddrInfForBy: state.main.stDepAddrInfForBy,
        stArrNameInfForBy: state.main.stArrNameInfForBy,
        DepInfForBy: state.main.DepInfForBy,
        ArrInfForBy: state.main.ArrInfForBy,
        userAr: state.main.userAr,
        noPlaceNum: state.main.noPlaceNum,
        showSeatsBlock: state.main.showSeatsBlock,
        matrix: state.main.matrix,
        seatsRows: state.main.seatsRows,
        seatsColumns: state.main.seatsColumns,
        fromID: state.main.fromID,
        toID: state.main.toID,
        sortMenu: state.main.sortMenu,
        tripListTwo: state.main.tripListTwo,
        resErrorRace: state.main.resErrorRace,
        resErrorRace2: state.main.resErrorRace2,
        nextDate: state.main.nextDate
    };
}

function mapDispatchToProps(dispatch) {
    return {
        mainAction: bindActionCreators(mainAction, dispatch),
    };
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Finder);
